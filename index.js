
const FIRST_NAME = "Alexandra";
const LAST_NAME = "Maftei";
const GRUPA = "1092";
/**
 * Make the implementation here
 */
function numberParser(x) {
    if ((isNaN(x)) || (x>Number.MAX_SAFE_INTEGER || x<Number.MIN_SAFE_INTEGER) || isFinite(x)==false)
        return NaN;
    else return parseInt(x);   
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

